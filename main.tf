# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
#  access_key = "AKIAZG4R2NPJ7GGDOXTS"        # for tfadmin user of AWS which will be used by terraform for provisioning
#  secret_key = "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"  # for tfadmin user of AWS which will be used by terraform for provisioning
}

# In this block we are creating a aws resource name "vpc" but format of using a resource is 
# <provider_name>_<resource_type> i.e aws_vpc  ( we can get this info from terraform aws documentation),
# we are also using variable development-vpc which # we will be using inside terraform as 
# reference for resource aws_vpc.
resource "aws_vpc" "development-vpc"{
#  cidr_block = "10.0.0.0/16"       # IP range to be used by AWS infrastructure created by terraform.
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name = "vpc-development"
    vpc_env = var.cidr_blocks[0].name
  }
}

variable "cidr_blocks" {
      description = "cider block for both vpc and subnet"     # This description will be shown while asking user to provide input.
      type = list(object({
            cidr_block = string
            name = string
      }))  # type is used to restrain user to provide input of a specific type only.
}

# In this block we are creating a AWS resource subnet i.e aws_subnet and giving it a name inside 
# Terraform as "dev-subnet-1"
resource "aws_subnet" "dev-subnet-1" {
      vpc_id = aws_vpc.development-vpc.id  # Providing reference of the VPC which is not yet created
  # from which this subnet would be created  aws-resource-type.resource-variable.id
      #cidr_block = "10.0.10.0/24"   # Ip range to be used by subnet i.e subset of IP's in VPC.
      cidr_block = var.cidr_blocks[1].cidr_block
      availability_zone = "ap-south-1a"   # Providing AZ-a
      tags = {
    Name = var.cidr_blocks[1].name
    }
  }


# This block is for quring the AWS to get the VPC detail using terraform.
data "aws_vpc" "existing_vpc" {   # providing the resource type for which we will query AWS and provide the 
# variable also in which the result of query will be saved
  default = true    # Arguments to be given which will be used as a search/filter criteria to get the 
# detail from AWS for that resource type i.e it should be default VPC
}

# Now in this Block we will be creating a subnet inside the VPC which was fetched from data query to AWS.
resource "aws_subnet" "dev-subnet-2"{
    vpc_id = data.aws_vpc.existing_vpc.id   # In this we are using "data" in the beginning to let terraform know 
# that this resource will be created from the result fetched from the data source and "id" attribute of VPC will be used 
# from the result from the data source ( existing_vpc)
    cidr_block = "172.31.48.0/20"   # Ip range to be used by subnet i.e subset of IP's in default VPC, also all 
  # subnets within the same VPC should not have any overlapping range.
    availability_zone = "ap-south-1a"
    tags = {
    Name = "subnet-2-default"
  }
}

# Note: 1 output belongs to only 1 attribute of resource we cannot define 1 output for multiple attribute's vales

output "dev-vpc-id" {        # name for the output block can be any variable 
     value = aws_vpc.development-vpc.id   # In this we are telling terraform which attribute's value of a resource
# we want it to output in the end of the apply command i.e it will output the "id" aatribute of the resource 
# we created ( i.e aws_vpc.development-vpc) we can execute the "terraform plan" in advance to know all attribute 
# of the resource we mentioned in our main.tf which are yet to be created.
}

output "dev-subnet-1-id" {        # name for the output block can be any variable 
     value = aws_subnet.dev-subnet-1.id
}
